######################################### AFwall+ Script ######################


warning:	
* this is a personal custum script for AFwall+
* requires root


include:	
* All 'afwall' chains/rules gets flushed automatically, before the custom script is executed
* Deny IPv6
* set an "good" DNS server 					# not Android 8+
  - but in homeWIFi use defaut	- set your own IP range in $HOMEWIFI
* set an "good" NTP server 					# not Android 8+
* set Orbot locl rules for route apps over Orbot
* routet apps:	
* Newpipe 		» org.schabi.newpipe
* AntennaPod 		» de.danoeh.antennapod
* SpaRSS 		» net.etuldan.sparss.floss
* Transportr 		» de.grobox.liberario
* Android Media Server 	(Default UID: 1013)
* blocks the company "Facebook" and "Google" IP's 		# not Android 8+

HowTo:
* https://gitlab.com/madbehaviorus/afwallscript/wikis/HowTo


# problems with Android 8 and after..
* allow UID 1000
* diable Mobile Connections in WiFi




################################################## HowTo #####################################################

install: 	- AFwall+ from F-Droid "https://f-droid.org/packages/dev.ukanth.ufirewall/" 
		  or compile it from here "https://github.com/ukanth/afwall"
		- Orbot from the F-Droid guardianproject repository or here 
		  "https://guardianproject.info/releases/Orbot-16.1.2-RC-2-tor-0.4.1.5-rc-fullperm-x86_64-release.apk" 
		- please verify: "https://guardianproject.info/releases/Orbot-16.1.2-RC-2-tor-0.4.1.5-rc-fullperm-x86_64-release.apk.asc" 

setup AFwall+	- "settings" -> "Rules/Connectivity Preferences" -> activate "Active Rules"; "Roaming-"; "LAN-" and  "VPN Control" 
		  and don't touch any IPv6 settings in there
		- "Binaries Preference" -> "Iptables binary" - > activate "System Iptables"
		- "Binaries Preference" -> "BusyBox binary" - > activate "Built-in BusyBox"
		- "Binaries Preference" -> "IDNS proxy" - > activate "Enable DNS via netd"
		  if you havn't activate it, many DNS requests aren't blocked
		- If you need "Multible Profiles" you must activate "Apply rules on profile switch"
		- now you must do a check on every app you want
		- and don't forget to check the "Linux Kernel" 
		  you need it for the DNS requests for netd
		- copy the start- and stop_script to the internal storeage
		- click "Custom Scripts" 
		  1. field -> paste the path to the start script
		  e.g. ". /storage/emulated/0/Android/data/AFwall+Scripts/afwall_start.sh"
		  2. field: -> paste the path to the start script
		  e.g. ". /storage/emulated/0/Android/data/AFwall+Scripts/afwall_stop.sh"
		  don't forget the dot and the space ". "
		  press OK

setup Orbot	- don't touch the onion before
		- "Settings" -> check "Use Default Iptables"
		- touch the onion
