###############################
## afwall_start.sh           ##
## AFWall+                   ##
## additional firewall rules ##
##                           ##
###############################
##                           ##
## All_for_One               ##
## Changes: 16.02.2019       ##
##                           ##
###############################

# set WLAN
WLANHOME='192.168.178.0/24'
# set DNS Server - Digitalcourage e.V. 46.182.19.48 (without DoT) / 5.9.164.112 (for DoT) - dismail 80.241.218.68
DNS=5.9.164.112
# set NTP Server - ntp0.fau.de - Location: University Erlangen-Nuernberg
NTP=131.188.3.222

####################
# Tweaks #
####################
## Kernel # Disable IPv6
echo 0 > /proc/sys/net/ipv6/conf/wlan0/accept_ra
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6
# Privacy IPv6 Address
echo 2 > /proc/sys/net/ipv6/conf/all/use_tempaddr
echo 2 > /proc/sys/net/ipv6/conf/default/use_tempaddr
## System
# Disable Global NTP Server
settings put global ntp_server 127.0.0.1
##  Disable Captive Portal check
# Since Android 7 and later
#
#settings put global captive_portal_detection_enabled 0
#settings put global captive_portal_server localhost
#settings put global captive_portal_mode 0

# Android 8 -> terminal emulator
# 
#su
#su
#pm disable com.android.captiveportallogin
#settings put global captive_portal_detection_enabled 0
#settings put global captive_portal_server localhost
#settings put global captive_portal_mode 0
# 
# reboot

####################
# iptables #
####################
IPTABLES=/system/bin/iptables
IP6TABLES=/system/bin/ip6tables
####################
# Defaults #
####################
# IPv4 connections
$IPTABLES -P INPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -P OUTPUT DROP
# IPv6 connections
$IP6TABLES -P INPUT DROP
$IP6TABLES -P FORWARD DROP
$IP6TABLES -P OUTPUT DROP
#####################
# Special Rules #
#####################
# Allow loopback interface lo
$IPTABLES -A INPUT -i lo -j ACCEPT
$IPTABLES -A "afwall" -o lo -j ACCEPT
# Set a specific DNS-Server for all networks  except home WLAN
$IPTABLES -t nat -I OUTPUT ! -s $WLANHOME -p tcp --dport 53 -j DNAT  --to-destination $DNS:53
$IPTABLES -t nat -I OUTPUT ! -s $WLANHOME -p udp --dport 53 -j DNAT  --to-destination $DNS:53
# Force a specific NTP Server
$IPTABLES -t nat -A OUTPUT -p tcp --dport 123 -j DNAT --to-destination  $NTP:123
$IPTABLES -t nat -A OUTPUT -p udp --dport 123 -j DNAT --to-destination  $NTP:123
#####################
# Incoming Traffic #
#####################
# Allow all traffic from an established connection
$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
# reject all not allowed incomming packages
$IPTABLES -A INPUT -p tcp -j REJECT --reject-with tcp-reset
$IPTABLES -A INPUT -j REJECT --reject-with icmp-port-unreachable


## Route Apps over Orbot
# Enable Apps to access Orbot
# Now it is better to use this in the AfWall+ GUI
#$IPTABLES -A "afwall" -d 127.0.0.1 -p tcp --dport 9040 -j ACCEPT
#$IPTABLES -A "afwall" -d 127.0.0.1 -p udp --dport 5400 -j ACCEPT


### set Specific route for an App to Orbot over AFwall+ ###




