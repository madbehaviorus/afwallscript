###############################
## afwall_start.sh           ##
## AFWall+                   ##
## additional firewall rules ##
##                           ##
###############################
##                           ##
## All_for_One               ##
## Changes: 2018-06-03       ##
##                           ##
###############################

## Set variables
IPTABLES=/system/bin/iptables
IP6TABLES=/system/bin/ip6tables
WIFIHOME='192.168.72.0/24' # set your own home WiFi

## All 'afwall' chains/rules gets flushed automatically, before the custom script is executed
# Flush/Purge all rules expect OUTPUT (quits with error)
$IPTABLES -F INPUT
$IPTABLES -F FORWARD
$IPTABLES -t nat -F
$IPTABLES -t mangle -F
$IP6TABLES -F INPUT
$IP6TABLES -F FORWARD
$IP6TABLES -t nat -F
$IP6TABLES -t mangle -F
# Flush/Purge all chains
$IPTABLES -X
$IPTABLES -t nat -X
$IPTABLES -t mangle -X
$IP6TABLES -X
$IP6TABLES -t nat -X
$IP6TABLES -t mangle -X

## Deny IPv6 connections
$IP6TABLES -P INPUT DROP
$IP6TABLES -P FORWARD DROP
$IP6TABLES -P OUTPUT DROP

##  Disable Captive Portal check
# Since Android 7 and later
settings put global captive_portal_detection_enabled 0
settings put global captive_portal_server localhost
settings put global captive_portal_mode 0

## Set new DNS Server - Digitalcourage
#$IPTABLES -t nat -I OUTPUT -p tcp --dport 53 -j DNAT --to-destination 46.182.19.48:53
#$IPTABLES -t nat -I OUTPUT -p udp --dport 53 -j DNAT --to-destination 46.182.19.48:53

# Set new DNS Server for all networks except home WiFi  - Digitalcourage
$IPTABLES -t nat -I OUTPUT ! -s $WIFIHOME -p tcp --dport 53 -j DNAT --to-destination 46.182.19.48:53
$IPTABLES -t nat -I OUTPUT ! -s $WIFIHOME -p udp --dport 53 -j DNAT --to-destination 46.182.19.48:53

## Set new NTP Server
# Force a specific NTP (ntp0.fau.de), Location: University Erlangen-Nuernberg
$IPTABLES -t nat -A OUTPUT -p tcp --dport 123 -j DNAT --to-destination 131.188.3.222:123
$IPTABLES -t nat -A OUTPUT -p udp --dport 123 -j DNAT --to-destination 131.188.3.222:123
# Disable Global NTP Server
settings put global ntp_server 127.0.0.1



## Route Apps over Orbot
# Enable Apps to access Orbot
$IPTABLES -A "afwall" -d 127.0.0.1 -p tcp --dport 9040 -j ACCEPT
$IPTABLES -A "afwall" -d 127.0.0.1 -p udp --dport 5400 -j ACCEPT

# Newpipe » org.schabi.newpipe
NEWPIPE_UID=`dumpsys package org.schabi.newpipe | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $NEWPIPE_UID -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $NEWPIPE_UID -j DNAT --to-destination 127.0.0.1:5400

# Android Media Server (Default UID: 1013)
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner 1013 -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner 1013 -j DNAT --to-destination 127.0.0.1:5400

# AntennaPod » de.danoeh.antennapod
ANTENNAPOD_UID=`dumpsys package de.danoeh.antennapod | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $ANTENNAPOD_UID -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $ANTENNAPOD_UID -j DNAT --to-destination 127.0.0.1:5400

# Transportr » de.grobox.liberario
TRANSPORTR_UID=`dumpsys package de.grobox.liberario | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`  
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $TRANSPORTR_UID -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $TRANSPORTR_UID -j DNAT --to-destination 127.0.0.1:5400

# DroidShows » nl.asymmetrics.droidshows
DROIDSHOWS_UID=`dumpsys nl.asymmetrics.droidshows | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $DROIDSHOWS_UID -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $DROIDSHOWS_UID -j DNAT --to-destination 127.0.0.1:5400

# SpaRSS » net.etuldan.sparss.floss
SPARSS_UID=`dumpsys package net.etuldan.sparss.floss | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
#$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $SPARSS_UID -j DNAT --to-destination 127.0.0.1:9040
#$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $SPARSS_UID -j DNAT --to-destination 127.0.0.1:5400

# Feeder » com.nononsenseapps.feeder
FEEDER_UID=`dumpsys package com.nononsenseapps.feeder | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
#$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $FEEDER_UID -j DNAT --to-destination 127.0.0.1:9040
#$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $FEEDER_UID -j DNAT --to-destination 127.0.0.1:5400

# Readify » ahmaabdo.readify.rss
READIFY_UID=`dumpsys package ahmaabdo.readify.rss | grep userId= | cut -d= -f2 - | cut -d' ' -f1 -`
$IPTABLES -t nat -A OUTPUT -p tcp -m owner --uid-owner $READIFY_UID -j DNAT --to-destination 127.0.0.1:9040
$IPTABLES -t nat -A OUTPUT -p udp -m owner --uid-owner $READIFY_UID -j DNAT --to-destination 127.0.0.1:5400


## Company Block-List###
# Company: Facebook
$IPTABLES -A "afwall" -d 31.13.24.0/21 -j REJECT
$IPTABLES -A "afwall" -d 31.13.64.0/18 -j REJECT
$IPTABLES -A "afwall" -d 45.64.40.0/22 -j REJECT
$IPTABLES -A "afwall" -d 66.220.144.0/20 -j REJECT
$IPTABLES -A "afwall" -d 69.63.176.0/20 -j REJECT
$IPTABLES -A "afwall" -d 69.171.224.0/19 -j REJECT
$IPTABLES -A "afwall" -d 74.119.76.0/22 -j REJECT
$IPTABLES -A "afwall" -d 103.4.96.0/22 -j REJECT
$IPTABLES -A "afwall" -d 157.240.0.0/17 -j REJECT
$IPTABLES -A "afwall" -d 173.252.64.0/18 -j REJECT
$IPTABLES -A "afwall" -d 179.60.192.0/22 -j REJECT
$IPTABLES -A "afwall" -d 185.60.216.0/22 -j REJECT
$IPTABLES -A "afwall" -d 204.15.20.0/22 -j REJECT

# Company: Google
$IPTABLES -A "afwall" -d 8.8.4.0/24 -j REJECT
$IPTABLES -A "afwall" -d 8.8.8.0/24 -j REJECT
$IPTABLES -A "afwall" -d 8.34.208.0/20 -j REJECT
$IPTABLES -A "afwall" -d 8.35.192.0/20 -j REJECT
$IPTABLES -A "afwall" -d 23.236.48.0/20 -j REJECT
$IPTABLES -A "afwall" -d 23.251.128.0/19 -j REJECT
$IPTABLES -A "afwall" -d 35.184.0.0/12 -j REJECT
$IPTABLES -A "afwall" -d 35.200.0.0/14 -j REJECT
$IPTABLES -A "afwall" -d 35.204.0.0/15 -j REJECT
$IPTABLES -A "afwall" -d 63.88.73.0/24 -j REJECT
$IPTABLES -A "afwall" -d 63.117.14.0/24 -j REJECT
$IPTABLES -A "afwall" -d 63.117.68.0/24 -j REJECT
$IPTABLES -A "afwall" -d 63.117.215.0/24 -j REJECT
$IPTABLES -A "afwall" -d 64.9.224.0/23 -j REJECT
$IPTABLES -A "afwall" -d 64.9.227.0/23 -j REJECT
$IPTABLES -A "afwall" -d 64.9.229.0/24 -j REJECT
$IPTABLES -A "afwall" -d 64.9.232.0/20 -j REJECT
$IPTABLES -A "afwall" -d 64.9.248.0/21 -j REJECT
$IPTABLES -A "afwall" -d 64.15.112.0/22 -j REJECT
$IPTABLES -A "afwall" -d 64.15.117.0/22 -j REJECT
$IPTABLES -A "afwall" -d 64.15.124.0/23 -j REJECT
$IPTABLES -A "afwall" -d 64.15.126.0/24 -j REJECT
$IPTABLES -A "afwall" -d 64.233.160.0/19 -j REJECT
$IPTABLES -A "afwall" -d 65.199.32.0/24 -j REJECT
$IPTABLES -A "afwall" -d 66.102.0.0/20 -j REJECT
$IPTABLES -A "afwall" -d 66.249.64.0/19 -j REJECT
$IPTABLES -A "afwall" -d 70.32.128.0/19 -j REJECT
$IPTABLES -A "afwall" -d 72.14.192.0/18 -j REJECT
$IPTABLES -A "afwall" -d 74.114.24.0/21 -j REJECT
$IPTABLES -A "afwall" -d 74.125.0.0/16 -j REJECT
$IPTABLES -A "afwall" -d 89.207.224.0/24 -j REJECT
$IPTABLES -A "afwall" -d 103.86.148.0/24 -j REJECT
$IPTABLES -A "afwall" -d 104.132.0.0/14 -j REJECT
$IPTABLES -A "afwall" -d 104.154.0.0/15 -j REJECT
$IPTABLES -A "afwall" -d 104.196.0.0/14 -j REJECT
$IPTABLES -A "afwall" -d 104.237.160.0/23 -j REJECT
$IPTABLES -A "afwall" -d 104.237.162.0/24 -j REJECT
$IPTABLES -A "afwall" -d 104.237.172.0/24 -j REJECT
$IPTABLES -A "afwall" -d 104.237.174.0/24 -j REJECT
$IPTABLES -A "afwall" -d 104.237.188.0/22 -j REJECT
$IPTABLES -A "afwall" -d 107.167.160.0/19 -j REJECT
$IPTABLES -A "afwall" -d 107.178.192.0/18 -j REJECT
$IPTABLES -A "afwall" -d 108.59.80.0/20 -j REJECT
$IPTABLES -A "afwall" -d 108.170.192.0/18 -j REJECT
$IPTABLES -A "afwall" -d 108.177.0.0/17 -j REJECT
$IPTABLES -A "afwall" -d 130.211.0.0/16 -j REJECT
$IPTABLES -A "afwall" -d 142.250.0.0/15 -j REJECT
$IPTABLES -A "afwall" -d 146.148.0.0/17 -j REJECT
$IPTABLES -A "afwall" -d 162.216.148.0/22 -j REJECT
$IPTABLES -A "afwall" -d 162.222.176.0/21 -j REJECT
$IPTABLES -A "afwall" -d 172.102.8.0/21 -j REJECT
$IPTABLES -A "afwall" -d 172.110.32.0/21 -j REJECT
$IPTABLES -A "afwall" -d 172.217.0.0/16 -j REJECT
$IPTABLES -A "afwall" -d 172.253.0.0/16 -j REJECT
$IPTABLES -A "afwall" -d 173.194.0.0/16 -j REJECT
$IPTABLES -A "afwall" -d 173.255.112.0/20 -j REJECT
$IPTABLES -A "afwall" -d 185.25.28.0/23 -j REJECT
$IPTABLES -A "afwall" -d 185.150.148.0/22 -j REJECT
$IPTABLES -A "afwall" -d 192.104.160.0/23 -j REJECT
$IPTABLES -A "afwall" -d 192.119.28.0/24 -j REJECT
$IPTABLES -A "afwall" -d 192.158.28.0/22 -j REJECT
$IPTABLES -A "afwall" -d 192.178.0.0/15 -j REJECT
$IPTABLES -A "afwall" -d 194.122.80.0/24 -j REJECT
$IPTABLES -A "afwall" -d 194.122.82.0/24 -j REJECT
$IPTABLES -A "afwall" -d 199.192.112.0/22 -j REJECT
$IPTABLES -A "afwall" -d 199.223.232.0/21 -j REJECT
$IPTABLES -A "afwall" -d 207.223.160.0/20 -j REJECT
$IPTABLES -A "afwall" -d 208.65.152.0/24 -j REJECT
$IPTABLES -A "afwall" -d 208.65.155.0/24 -j REJECT
$IPTABLES -A "afwall" -d 208.68.108.0/22 -j REJECT
$IPTABLES -A "afwall" -d 208.81.188.0/22 -j REJECT
$IPTABLES -A "afwall" -d 208.117.225.0/22 -j REJECT
$IPTABLES -A "afwall" -d 208.117.229.0/24 -j REJECT
$IPTABLES -A "afwall" -d 208.117.233.0/24 -j REJECT
$IPTABLES -A "afwall" -d 208.117.243.0/23 -j REJECT
$IPTABLES -A "afwall" -d 208.117.246.0/24 -j REJECT
$IPTABLES -A "afwall" -d 208.117.253.0/24 -j REJECT
$IPTABLES -A "afwall" -d 209.85.128.0/17 -j REJECT
$IPTABLES -A "afwall" -d 209.107.176.0/20 -j REJECT
$IPTABLES -A "afwall" -d 216.58.192.0/19 -j REJECT
$IPTABLES -A "afwall" -d 216.73.80.0/20 -j REJECT
$IPTABLES -A "afwall" -d 216.239.32.0/19 -j REJECT
$IPTABLES -A "afwall" -d 216.252.220.0/22 -j REJECT

